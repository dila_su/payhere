#import "PayherePlugin.h"
#if __has_include(<payhere/payhere-Swift.h>)
#import <payhere/payhere-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "payhere-Swift.h"
#endif

@implementation PayherePlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftPayherePlugin registerWithRegistrar:registrar];
}
@end
