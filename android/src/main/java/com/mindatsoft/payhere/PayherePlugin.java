package com.mindatsoft.payhere;

import android.app.Activity;

import androidx.annotation.NonNull;

import java.util.Map;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.Registrar;

/** PayherePlugin */
public class PayherePlugin implements FlutterPlugin, MethodCallHandler, ActivityAware {


  /// when the Flutter Engine is detached from the Activity
  private PayhereDelegate payhereDelegate;
  private ActivityPluginBinding pluginBinding;
  private MethodChannel channel;


//  /**
//   * Constructor for Flutter version < 1.12
//   * @param registrar
//   */
//  private PayhereFlutterPlugin(Registrar registrar) {
//    this.payhereDelegate = new PayhereDelegate(registrar.activity());
//    registrar.addActivityResultListener(payhereDelegate);
//  }

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    channel = new MethodChannel(flutterPluginBinding.getFlutterEngine().getDartExecutor(), "payhere");
    channel.setMethodCallHandler(this);
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {

  }

  // This static function is optional and equivalent to onAttachedToEngine. It supports the old
  // pre-Flutter-1.12 Android projects. You are encouraged to continue supporting
  // plugin registration via this function while apps migrate to use the new Android APIs
  // post-flutter-1.12 via https://flutter.dev/go/android-project-migration.
  //
  // It is encouraged to share logic between onAttachedToEngine and registerWith to keep
  // them functionally equivalent. Only one of onAttachedToEngine or registerWith will be called
  // depending on the user's project. onAttachedToEngine or registerWith must both be defined
  // in the same class.
  public static void registerWith(Registrar registrar) {
    final MethodChannel channel = new MethodChannel(registrar.messenger(), "payhere");
    channel.setMethodCallHandler(new PayherePlugin());
  }



  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    switch (call.method) {

      case "onTimePaymentDemo":
        System.out.println("====================== in android");
        System.out.println(call.arguments.toString());
        System.out.println("====================== in android");
        System.out.println(payhereDelegate==null);
        payhereDelegate.onTimePaymentDemo((Map<String, Object>) call.arguments, result);
        break;
      case "onTimePaymentReal" :
        payhereDelegate.onTimePaymentReal((Map<String, Object>) call.arguments, result);
        break;

      case "getPlatformVersion":
        result.success("Android " + android.os.Build.VERSION.RELEASE);
        break;

      default:
        result.notImplemented();
    }
  }



  @Override
  public void onAttachedToActivity(@NonNull ActivityPluginBinding binding) {
    System.out.println("))))))))))))))))))))))))))))");
    this.payhereDelegate = new PayhereDelegate(binding.getActivity());
    this.pluginBinding = binding;
    binding.addActivityResultListener(payhereDelegate);
  }

  @Override
  public void onDetachedFromActivityForConfigChanges() {
    onDetachedFromActivity();
  }

  @Override
  public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding binding) {
    onAttachedToActivity(binding);
  }

  @Override
  public void onDetachedFromActivity() {
    pluginBinding.removeActivityResultListener(payhereDelegate);
    pluginBinding = null;
  }
}
