import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:payhere/payhere.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  InitRequest req = InitRequest();
  PHConfigs configs = PHConfigs();
  String responseText="";
  String _platformVersion = 'Unknown';

  @override
  void initState() {
    super.initState();
    initPlatformState();


  }



  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: new Column(
            children: [
              Text('Running on: $_platformVersion\n'),
              new RaisedButton(onPressed: ()async{
                var phResponse = await Payhere.oneTimePaymentReal(request: req);
                print('============');
              print( phResponse.toJson().toString());
            })
            ],
          )
        ),
      ),
    );
  }

  initPlatformState() {
    req.setMerchantId("1215005"); // Your Merchant PayHere ID
    req.setMerchantSecret(
        "4Ocfbcwadsr4DpZpBS5W578n2zXcYWTF28he1flJ1y52");  // Your Merchant secret (Add your app at Settings > Domains & Credentials, to get this))
    req.setCurrency("LKR"); // Currency code LKR/USD/GBP/EUR/AUD
    req.setAmount(1000.00); // Final Amount to be charged
    req.setOrderId("230000123"); // Unique Reference ID
    req.setItemsDescription("Door bell wireless"); // Item description title
    req.setCustom1("This is the custom message 1");
    req.setCustom2("This is the custom message 2");
    req.getCustomer().setFirstName("Saman");
    req.getCustomer().setLastName("Perera");
    req.getCustomer().setEmail("samanp@gmail.com");
    req.getCustomer().setPhone("+94771234567");
    req.getCustomer().getAddress().setAddress("No.1, Galle Road");
    req.getCustomer().getAddress().setCity("Colombo");
    req.getCustomer().getAddress().setCountry("Sri Lanka");

//Optional Params
    req.getCustomer().getDeliveryAddress().setAddress("No.2, Kandy Road");
    req.getCustomer().getDeliveryAddress().setCity("Kadawatha");
    req.getCustomer().getDeliveryAddress().setCountry("Sri Lanka");

    req
        .getItems()
        .add(Item.create(id: null, name: "demo", quantity: 4, amount: 45.56));
  }
}
