
import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:payhere/models/phresponse.dart';

import 'models/initRequest.dart';

export 'package:payhere/const/phconfigs.dart';
export 'package:payhere/const/phconstants.dart';
export 'package:payhere/models/initRequest.dart';
export 'package:payhere/models/item.dart';
export 'package:payhere/models/phresponse.dart';

class Payhere {
  static const MethodChannel _channel =
      const MethodChannel('payhere');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<PhResponse> oneTimePaymentSandbox(
      {@required InitRequest request}) async {
    try {
      Map<String, dynamic> map = request.toJson();
      print('====');
      PhResponse phResponse = await _channel
          .invokeMethod('onTimePaymentDemo', map)
          .then(
              (value) => PhResponse.fromJson(json.decode(value['response'])));
      return phResponse;
    } catch (e) {
      debugPrint(e.toString());
      return null;
    }
  }

  static Future<PhResponse> oneTimePaymentReal(
      {@required InitRequest request}) async {
    try {
      Map<String, dynamic> map = request.toJson();
      PhResponse phResponse = await _channel
          .invokeMethod('onTimePaymentReal', map)
          .then(
              (value) => PhResponse.fromJson(json.decode(value['response'])));
      return phResponse;
    } catch (e) {
      debugPrint(e.toString());
      return null;
    }
  }
}
